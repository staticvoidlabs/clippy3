package managers

import (
	"bytes"
	"fmt"
	"image"
	"image/png"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

func generateThumbPNG(url string, videoID string) string {

	tmpCacheDir := getThumbnailCacheDir()

	// Create the file
	out, err := os.Create(tmpCacheDir + videoID + ".jpg")
	if err != nil {
		fmt.Println(err)
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	imgByte, _ := ioutil.ReadFile(tmpCacheDir + videoID + ".jpg")

	img, _, errDecode := image.Decode(bytes.NewReader(imgByte))
	if errDecode != nil {
		fmt.Println(errDecode)
	}

	outFilePNG, _ := os.Create(tmpCacheDir + videoID + ".png")
	_ = png.Encode(outFilePNG, img)

	tmpabsolutePath, err := filepath.Abs(tmpCacheDir + videoID + ".jpg")

	return tmpabsolutePath
}

func SaveYoutubeDLOutputToFile(output string) {

}
