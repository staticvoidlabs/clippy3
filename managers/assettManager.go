package managers

const cAppHeader = ""

// GetAssettText returns const string for a given assett id.
func GetAssettText(id string) string {

	tmpReturnString := "n/a"

	switch id {
	case "appHeader":
		tmpReturnString = cAppHeader
	}

	return tmpReturnString
}
