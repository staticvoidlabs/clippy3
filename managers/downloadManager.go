package managers

import (
	"fmt"
	"os/exec"
	"strings"

	"gitlab.com/staticvoidlabs/clippy3/models"
)

var mDownloadQueue [10]models.DownloadJob
var mDownloadHistory [99]models.DownloadJob
var mJobCounter int = 0

// DownloadVideo downloads the given video.
func downloadVideo(index int, jobQueue *[10]models.DownloadJob, currentConfig models.Config) {

	// Get videoID from URL.
	tmpStringArray := strings.Split(jobQueue[index].URL, "?v=")
	tmpVideoID := tmpStringArray[1]
	jobQueue[index].VideoID = tmpVideoID
	jobQueue[index].State = "INITIALIZED"
	jobQueue[index].StateHasChanged = true

	// Get details for current video.
	tmpCurrentVideo := GetVideosByID(tmpVideoID)
	jobQueue[index].VideoTitle = tmpCurrentVideo.Title
	jobQueue[index].FilePath = tmpCurrentVideo.FilePath + tmpCurrentVideo.FileName
	jobQueue[index].FileExtSet = false
	jobQueue[index].State = "DOWNLOADING"
	jobQueue[index].StateHasChanged = true
	//fmt.Print(" > (" + tmpCurrentVideo.ChannelTitle + ": " + tmpCurrentVideo.Title[:10] + "...) > downloading...")

	// Run pre-processing for video to be downloaded.
	tmpCurrentVideo = PreProcessVideoFile(tmpCurrentVideo, currentConfig)

	// Build download command string.
	cmdArg1 := jobQueue[index].URL
	cmdArg2 := currentConfig.ArgOutput
	cmdArg3 := tmpCurrentVideo.FilePath + tmpCurrentVideo.FileName + currentConfig.ArgTemplateName
	cmd := exec.Command(currentConfig.FullPathYtDl, cmdArg1, cmdArg2, cmdArg3)

	// Show toast (download started).
	//ShowToastOnDownloadStarted(tmpCurrentVideo)

	// Execute command and print output.
	out, err := cmd.CombinedOutput()

	if err != nil {
		jobQueue[index].State = "FAILED"
		jobQueue[index].StateHasChanged = true
		fmt.Println("cmd.Run() failed with %s\n", err)
	}

	if currentConfig.YtDlLoggingEnabled {
		SaveYoutubeDLOutputToFile(string(out))
		fmt.Println("Youtube-DL state: %s" + string(out))
	}

	// Run post-processing for downloaded video.
	tmpCurrentVideo = PostProcessVideoFile(tmpCurrentVideo)

	// Show toast (download finished).
	//ShowToastOnDownloadFinished(tmpCurrentVideo)

	jobQueue[index].State = "FINISHED"
	jobQueue[index].StateHasChanged = true
	//fmt.Println(" completed.")

	// Put completed job to history array.
	mDownloadHistory[mJobCounter] = jobQueue[index]
	mJobCounter++
}

func getDownloadSlot(currentJobs *[10]models.DownloadJob) int {

	tmpFirstFreeSlotIndex := -1

	for index, item := range currentJobs {

		if item.State == "IDLE" || item.State == "" {
			tmpFirstFreeSlotIndex = index
			break
		}

	}

	return tmpFirstFreeSlotIndex
}

func getRunningJobCount(currentJobs *[10]models.DownloadJob) int {

	tmpRunningJobs := 0

	for _, item := range currentJobs {

		if item.State != "IDLE" {
			tmpRunningJobs++
		}
	}

	return tmpRunningJobs
}

// BuildDownloadJob builds a job object and starts downloading the corresponding file.
func BuildDownloadJob(clipboardContent string, config models.Config) {

	var tmpNewJob models.DownloadJob
	tmpNewJob.Priority = 1
	tmpNewJob.State = "IDLE"
	tmpNewJob.StateInfo = "n/a"
	tmpNewJob.URL = clipboardContent
	tmpNewJob.VideoID = "n/a"

	tmpIndex := getDownloadSlot(&mDownloadQueue)

	if tmpIndex != -1 {

		mDownloadQueue[tmpIndex] = tmpNewJob
		downloadVideo(tmpIndex, &mDownloadQueue, config)

	}

}
