package models

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version                    string `json:"version"`
	ApiPort                    int    `json:"apiPort"`
	DebugLevel                 string `json:"debugLevel"`
	PathDownloads              string `json:"pathDownloads"`
	YtDlLoggingEnabled         bool   `json:"ytDlLoggingEnabled"`
	FullPathYtDl               string `json:"fullPathYtDl"`
	ArgOutput                  string `json:"argOutput"`
	ArgTemplateName            string `json:"argTemplateName"`
	ThumbailCacheDir           string `json:"thumbailCacheDir"`
	YoutubeAPIKey              string `json:"ytApiKey"`
}

// Video struct defines the data model to hold a video object.
type Video struct {
	ChannelTitle  string
	Title         string
	OriginalTitle string
	PublishedAt   string
	Duration      string
	FilePath      string
	FileName      string
	ThumbURL      string
	ThumbFilepath string
}

// Queue struct defines the data model to hold a download job object.
type Queue struct {
	Jobs []DownloadJob
}

// DownloadJob struct defines the data model to hold a download job object.
type DownloadJob struct {
	VideoID         string
	VideoTitle      string
	URL             string
	FilePath        string
	FileExtSet      bool
	FileSize        string
	Priority        int
	State           string
	StateInfo       string
	StateHasChanged bool
}

// JobResult struct defines the data model to hold the download job channel object.
type JobResult struct {
	VideoID  string
	JobState string
}
