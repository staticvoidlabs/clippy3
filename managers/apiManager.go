package managers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var mLastVideoParameter string = ""

// InitAPIService starts the API service.
func InitAPIService() {

	WriteLogMessage("Starting Web API services", true)

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/", getStateInfoService)
	router.HandleFunc("/clippy/queue", generateHTMLOutput)
	router.HandleFunc("/clippy/queue/log", showCurrentQueue)
	router.HandleFunc("/clippy/queue/add/{id}", addQueueItem)
	router.HandleFunc("/clippy/ctrl/restart", restartSubsystem)
	//router.PathPrefix("/nappy/news").Handler(http.StripPrefix("/nappy/news", http.FileServer(http.Dir("./www"))))

	tmpConfiguredApiPort := ":" + strconv.Itoa(mCurrentConfig.ApiPort)

	// Start rest service.
	go log.Fatal(http.ListenAndServe(tmpConfiguredApiPort, router))

}

// Endpoint implementation.
func getStateInfoService(w http.ResponseWriter, r *http.Request) {

	tmpLogMessages := GetCurrentLog()

	for _, message := range tmpLogMessages {
		w.Write([]byte(message + "\r\n"))
	}

}

func showCurrentQueue(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:showCurrentQueue", true)

	json.NewEncoder(w).Encode("VoidServices_response:show_current_queue:success.")
}

func updateRecordings(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:updateRecordings", true)

	json.NewEncoder(w).Encode("VoidServices_response:update_recordings:success.")

}

func getStateInfo(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:getStateInfo", true)

	json.NewEncoder(w).Encode("VoidServices_response:get_state_info:success.")
}

func restartSubsystem(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:restartSubsystem", true)

	json.NewEncoder(w).Encode("VoidServices_response:restart_subsystem:success.")
}

func generateHTMLOutput(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:generateHTMLOutput", true)

	json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:success.")
}

func addQueueItem(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:addQueueItem", true)

	// Get videoID parameter.
	params := mux.Vars(r)
	videoID, _ := params["id"]

	processDownloadRequest(videoID)

	json.NewEncoder(w).Encode("VoidServices_response:add_queue_item:success.")

}

func htmlTesting(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:testing", true)

	//json.NewEncoder(w).Encode("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
	fmt.Print("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
}

func processDownloadRequest(videoID string) {

	if mLastVideoParameter != videoID {

		mLastVideoParameter = videoID

		// Get first 23 chars of current clipboard content.
		tmpRunes := []rune(videoID)
		tmpSubstringStart := string(tmpRunes[0:23])

		if tmpSubstringStart != "https://www.youtube.com" {
			//mLastAction = "skipping: " + tmpClipboardContent
			return
		}

		go BuildDownloadJob(videoID, mCurrentConfig)
	}
}
