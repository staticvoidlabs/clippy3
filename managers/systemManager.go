package managers

import (
	"fmt"
	"os"

	"gitlab.com/staticvoidlabs/clippy3/models"
)

const colorReset = "\033[0m"
const colorRed = "\033[31m"
const colorGreen = "\033[32m"
const colorYellow = "\033[33m"
const colorBlue = "\033[34m"
const colorPurple = "\033[35m"
const colorCyan = "\033[36m"
const colorWhite = "\033[97m"
const colorGray = "\033[37m"

var mStateHasChanged bool

func getFileSize(file string) string {

	tmpSize := "n/a"

	fi, err := os.Stat(file)
	if err != nil {
		fmt.Println(err)
	}
	// get the size
	size := fi.Size()
	fmt.Println(size)

	return tmpSize
}

func setFileExtension(job *models.DownloadJob) {

	tmpStringMkv := job.FilePath + ".mkv"
	tmpStringMp4 := job.FilePath + ".mp4"
	tmpStringWebm := job.FilePath + ".webm"
	tmpStringavi := job.FilePath + ".avi"

	if fileExists(tmpStringMkv) {
		job.FilePath = tmpStringMkv
		job.FileExtSet = true
	} else if fileExists(tmpStringMp4) {
		job.FilePath = tmpStringMp4
		job.FileExtSet = true
	} else if fileExists(tmpStringWebm) {
		job.FilePath = tmpStringWebm
		job.FileExtSet = true
	} else if fileExists(tmpStringavi) {
		job.FilePath = tmpStringavi
		job.FileExtSet = true
	}

}
