package managers

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/staticvoidlabs/clippy3/models"
)

// BuildArgumentString builds the arguments string for the video to download.
func BuildArgumentString(currentClipboardContent string, currentConfig models.Config) string {

	//tmpReturnString := currentClipboardContent + " " + currentConfig.ArgOutput + " \"" + currentConfig.PathDownloads + currentConfig.ArgTemplateName + "\""

	tmpReturnString := currentClipboardContent + " " + currentConfig.ArgOutput + " \"" + currentConfig.PathDownloads + "testfile\""

	return tmpReturnString
}

// PreProcessVideoFile updates the file name before downloadung it to fit file name conventions.
func PreProcessVideoFile(currentVideo models.Video, currentConfig models.Config) models.Video {

	// Remove unwanted chars in video title.
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, ":", "_")
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, ",", "_")
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, ".", "_")
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, "?", "_")
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, "*", "_")
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, "\"", "")
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, "|", "_")
	currentVideo.Title = strings.ReplaceAll(currentVideo.Title, "�", "ä")

	// Set video filepath.
	currentVideo.FilePath = currentConfig.PathDownloads
	currentVideo.FileName = currentVideo.Title

	return currentVideo
}

// PostProcessVideoFile updates the file name depending on the video channel.
func PostProcessVideoFile(currentVideo models.Video) models.Video {

	// Update current video object.
	tmpStringMkv := currentVideo.FilePath + currentVideo.FileName + ".mkv"
	tmpStringMp4 := currentVideo.FilePath + currentVideo.FileName + ".mp4"
	tmpStringWebm := currentVideo.FilePath + currentVideo.FileName + ".webm"
	tmpStringavi := currentVideo.FilePath + currentVideo.FileName + ".avi"

	if fileExists(tmpStringMkv) {
		currentVideo.FileName = currentVideo.FileName + ".mkv"
	} else if fileExists(tmpStringMp4) {
		currentVideo.FileName = currentVideo.FileName + ".mp4"
	} else if fileExists(tmpStringWebm) {
		currentVideo.FileName = currentVideo.FileName + ".webm"
	} else if fileExists(tmpStringavi) {
		currentVideo.FileName = currentVideo.FileName + ".avi"
	} else {
		return currentVideo
	}

	// Set channel identifier depending on videos channel.
	tmpChannelIdentifier := ""

	switch currentVideo.ChannelTitle {
	case "Rocket Beans TV":
		tmpChannelIdentifier = "[RBTV] "
	case "Game Two":
		tmpChannelIdentifier = "[GAMETWO] "
	case "Rocket Beans Gaming":
		tmpChannelIdentifier = "[RBTV] "
	case "Rocket Beans Let’s Play & Streams":
		tmpChannelIdentifier = "[RBTV] "
	case "WALULIS":
		tmpChannelIdentifier = "[WALULIS] "
	case "WALULIS DAILY":
		tmpChannelIdentifier = "[WALULIS] "
	case "WALULIS STORY - SWR3":
		tmpChannelIdentifier = "[WALULIS] "
	case "PULS Reportage":
		tmpChannelIdentifier = "[PULS] "
	case "Das schaffst du nie!":
		tmpChannelIdentifier = "[DSDN] "
	case "Quarks":
		tmpChannelIdentifier = "[QUARKS] "
	default:
		tmpChannelIdentifier = "[OTHER] "
	}

	// Rename file.
	tmpFileNameOld := currentVideo.FilePath + currentVideo.FileName
	tmpFileNameNew := currentVideo.FilePath + tmpChannelIdentifier + currentVideo.FileName
	err := os.Rename(tmpFileNameOld, tmpFileNameNew)

	if err != nil {
		fmt.Println(err)
	}

	//fmt.Println("Renamed file " + tmpFileNameOld + " to " + tmpFileNameNew)

	return currentVideo
}

func fileExists(filename string) bool {

	info, err := os.Stat(filename)

	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}

// RunYoutvPostProcessing checks for existing Youtv videos in download folder and performs file name optimization.
func RunYoutvPostProcessing(currentConfig models.Config) {

	// Get files in download folder.
	tmpFiles, err := ioutil.ReadDir(currentConfig.PathDownloads)

	if err != nil {
		fmt.Println(err)
	}

	for _, file := range tmpFiles {

		if file.Mode().IsRegular() {

			tmpFileName := file.Name()

			if filepath.Ext(tmpFileName) == ".mkv" ||
				filepath.Ext(tmpFileName) == ".mp4" ||
				filepath.Ext(tmpFileName) == ".webm" ||
				filepath.Ext(tmpFileName) == ".avi" {

				// Check if file looks like a Youtv file name.
				if len(tmpFileName) > 8 && len(strings.Split(tmpFileName[0:9], "-")) == 3 {

					tmpFileNamePartArray := strings.Split(tmpFileName, ".")
					tmpFileNameWithoutExt := tmpFileNamePartArray[0]

					// Parse broadcast date
					tmpDatePartsArray := strings.Split(tmpFileNameWithoutExt[:16], "_")
					tmpTimeStamp := strings.Replace(tmpDatePartsArray[0], "-", "", -1) + "_" + strings.Replace(tmpDatePartsArray[1], "-", "", -1)

					// Get short title and origin.
					tmpFileNameWithoutExt = tmpFileNameWithoutExt[17:len(tmpFileNameWithoutExt)]
					tmpArray2 := strings.Split(tmpFileNameWithoutExt, "_")
					tmpFileNameWithoutExt = tmpArray2[0]
					tmpFileNameWithoutExt = strings.Replace(tmpFileNameWithoutExt, "-", " ", -1)
					tmpStationName := tmpArray2[1]
					tmpFileNameOptimzed := "[YOUTV] " + tmpFileNameWithoutExt + " (" + tmpStationName + "_" + tmpTimeStamp + ")." + tmpFileNamePartArray[1]

					// Rename file.
					tmpFileNameOld := currentConfig.PathDownloads + tmpFileName
					tmpFileNameNew := currentConfig.PathDownloads + tmpFileNameOptimzed

					err := os.Rename(tmpFileNameOld, tmpFileNameNew)

					if err != nil {
						fmt.Println(err)
					}

				}

			}

		}

	}

}
