package managers

import (
	"context"
	"fmt"

	"gitlab.com/staticvoidlabs/clippy3/models"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
)

var mError error

// GetVideosByID returns a list of videos for a given ID.
func GetVideosByID(videoID string) models.Video {

	//videoID = "1w2xlTMGVFU"

	var tmpReturnVideo models.Video

	ctx := context.Background()
	tmpYoutubeAPIKey := getYoutubeAPIKey()
	tmpYoutubeService, err := youtube.NewService(ctx, option.WithAPIKey(tmpYoutubeAPIKey))

	if err != nil {
		fmt.Println(err)
		return tmpReturnVideo
	}

	mContentArray := []string{"snippet,contentDetails"}

	tmpSearchVideoRequest := tmpYoutubeService.Videos.List(mContentArray)
	tmpSearchVideoRequest.Id(videoID)

	tmpSearchVideoResponse, err := tmpSearchVideoRequest.Do()

	if err != nil {
		fmt.Println(err)
		return tmpReturnVideo
	}

	if tmpSearchVideoResponse.Items != nil && tmpSearchVideoResponse.Items[0] != nil {

		tmpReturnVideo.ChannelTitle = tmpSearchVideoResponse.Items[0].Snippet.ChannelTitle
		tmpReturnVideo.Title = tmpSearchVideoResponse.Items[0].Snippet.Title
		tmpReturnVideo.OriginalTitle = tmpSearchVideoResponse.Items[0].Snippet.Title
		tmpReturnVideo.PublishedAt = tmpSearchVideoResponse.Items[0].Snippet.PublishedAt
		tmpReturnVideo.Duration = tmpSearchVideoResponse.Items[0].ContentDetails.Duration
		tmpReturnVideo.ThumbURL = tmpSearchVideoResponse.Items[0].Snippet.Thumbnails.Default.Url
		tmpReturnVideo.ThumbFilepath = generateThumbPNG(tmpReturnVideo.ThumbURL, videoID)

		if err != nil {
			panic(err)
		}

	}

	return tmpReturnVideo
}

// GetChannelsByID returns a list of channels for a given ID.
func _GetChannelsByID(channelID string, ytAPIKey string) {

	channelID = "UCBzai1GXVKDdVCrwlKZg_6Q"

	ctx := context.Background()
	tmpYoutubeService, err := youtube.NewService(ctx, option.WithAPIKey(ytAPIKey))

	if err != nil {
		fmt.Println(err)
	}

	mContentArray := []string{"snippet"}

	tmpSearchListRequest := tmpYoutubeService.Search.List(mContentArray)
	tmpSearchListRequest.Order("date")
	tmpSearchListRequest.MaxResults(25)
	tmpSearchListRequest.ChannelId(channelID)

	tmpSearchListResponse, err := tmpSearchListRequest.Do()

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(tmpSearchListResponse)
}
