package main

import (
	"fmt"

	"gitlab.com/staticvoidlabs/clippy3/managers"
	"gitlab.com/staticvoidlabs/clippy3/models"
)

var mShouldExit = false
var mCurrentConfig models.Config
var mLastAction string = ""

func main() {

	// Init config.
	mCurrentConfig = managers.GetCurrentConfig()

	fmt.Println(" Starting Clippy 3 (Version: " + mCurrentConfig.Version + ")")
	
	// Initailize GorillaMUX service and start listening...
	managers.InitAPIService()

}
